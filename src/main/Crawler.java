package main;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Vector;
import java.util.regex.Pattern;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import edu.uci.ics.crawler4j.url.WebURL;

public class Crawler extends WebCrawler {

	private final static Pattern FILTERS = Pattern
			.compile(".*(\\.(css|js|bmp|gif|jpe?g"
					+ "|png|tiff?|mid|mp2|mp3|mp4"
					+ "|wav|avi|mov|mpeg|ram|m4v|pdf"
					+ "|rm|smil|wmv|swf|wma|zip|rar|gz))$");

	public static final String magnetFileName = "magnetList.txt";

	public static final String newMagnetsFileName = "magnetList-new.txt";

	public static final String rootUrl="https://www.epublibre.org";
	
	public static Vector<String> magnets = new Vector<String>();
	public static PrintWriter newMagnets = null;

	
	public static void main(String[] args) throws Exception {
				
		// Cargamos el archivo magnetsList.txt
		loadCurrentMagnets();

		//Inicializamos
		String crawlStorageFolder = "./tmp";
		int numberOfCrawlers = 7;

		CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(crawlStorageFolder);

		/*
		 * Instantiate the controller for this crawl.
		 */
		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig,
				pageFetcher);
		CrawlController controller = new CrawlController(config, pageFetcher,
				robotstxtServer);

		/*
		 * For each crawl, you need to add some seed urls. These are the first
		 * URLs that are fetched and then the crawler starts following links
		 * which are found in these pages
		 */
		controller.addSeed(rootUrl);

		/*
		 * Start the crawl. This is a blocking operation, meaning that your code
		 * will reach the line after this only when crawling is finished.
		 */
		// Abrimos el archivo para escirbir los nuevos magnets
		newMagnets = new PrintWriter(new File(newMagnetsFileName));
		controller.start(Crawler.class, numberOfCrawlers);
		newMagnets.close();
		newMagnets = new PrintWriter(new File(magnetFileName));
		for (String item : magnets)
			newMagnets.println(item);
		newMagnets.close();
	}
	
	private static void loadCurrentMagnets() {
		try {
			FileInputStream fstream = new FileInputStream(magnetFileName);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			while ((strLine = br.readLine()) != null) {
				magnets.add(strLine);
			}
			in.close();
			Collections.sort(magnets);
			// Quitar duplicados
			int dups = 0;
			if (magnets.size() > 1)
				for (int i = 0; i < magnets.size() - 1; i++) {
					if (magnets.get(i).equals(magnets.get(i + 1))) {
						magnets.remove(i);
						i--;
						dups++;
					}
				}
			System.out.println("Loaded " + magnets.size() + " links, removed "
					+ dups + " duplicates.");
		} catch (Exception e) {// Catch exception if any
			System.out.println("No magnets to load.");
		}
	}
	/**
	 * You should implement this function to specify whether the given url
	 * should be crawled or not (based on your crawling logic).
	 */
	public boolean shouldVisit(WebURL url) {
		String href = url.getURL().toLowerCase();
		if (FILTERS.matcher(href).matches())
			return false;
		if (!href.startsWith(rootUrl))
			return false;
		if (href.startsWith(rootUrl+"user/"))
			return false;
		if (href.startsWith(rootUrl+"taxonomy/term/"))
			return false;
		if (href.startsWith(rootUrl+"forum"))
			return false;
		if (href.startsWith(rootUrl+"modules"))
			return false;
		if (href.startsWith(rootUrl+"valorados"))
			return false;
		if (href.startsWith(rootUrl+"proximamente"))
			return false;
		if (href.startsWith(rootUrl+"populares"))
			return false;
		if (href.startsWith(rootUrl+"sites/"))
			return false;
		if (href.contains("?page=page%3Dpage%3Dpage%3Dpage%3Dpage%3Dpage%3Dpage"
				.toLowerCase()))
			return false;
		if (href.startsWith(rootUrl+"node/69?page="))
			return false;
		return true;
	}

	/**
	 * This function is called when a page is fetched and ready to be processed
	 * by your program.
	 */
	@Override
	public void visit(Page page) {
		String url = page.getWebURL().getURL();
		System.out.println("URL: " + url.substring(rootUrl.length()));
		if (page.getParseData() instanceof HtmlParseData) {
			String cnt;
			try {
				cnt = new String(page.getContentData(),page.getContentCharset());
				int index = cnt.indexOf("\"magnet:");
				if (index > 0) {
					int endIndex = cnt.indexOf("\"", index + 1);
					String link = cnt.substring(index + 1, endIndex);
					int position = Collections.binarySearch(magnets, link);
					if (position < 0) {
						System.out.println(link.substring(0, 75) + "���");
						newMagnets.println(link);
						magnets.add(-position - 1, link);
					}
				}
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}